## [Weather Progressive App]
A local weather app that fetches weather forecast from Openweathermap.org. A Progressive Web App built with Vue.js.

## Features
* Progressive Web App
* Offline mode
* Add to homescreen (mobile)
* Different theme depending on time (day or night)
* Different weather icon depending on weather
* Click to toggle temperature scale (Celsius or Fahrenheit)

## Built With
* Vue.js
* Babel

# Build
`npm run build`

# Run
`npm run serve`
