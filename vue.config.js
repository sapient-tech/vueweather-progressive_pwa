module.exports = {
    baseUrl: '/progressive-weather-app/',
    pwa: {
        themeColor: '#00c0c9',
        msTileColor: '#139999'
    }
}
